# Zeptolab App

Zeptolab app is multi module maven project with [Spring-boot](https://spring.io/) (as application context initializer)
and [Netty](https://netty.io/) (server and sockets)


* Java version : 17
* Maven version : Apache Maven 3.8.7
* Spring boot version : 3.0.6

### Maven Modules

```
zeptolab-server
zeptolab-services
zeptolab-persistence
```

**zeptolab-server:** Contains spring boot application and netty server

**zeptolab-services:** Contains common business logic related classes and functionalities

**zeptolab-persistence:** Contains DB models. and data access repositories

### Application configurations and properties.

**DB:** In memory [H2-database](https://www.h2database.com/html/main.html) with [Flyway](https://flywaydb.org/)
migration support.
Flyway migration script can be found under zeptolab-persistence module [db/migration]() directory
with [V0_0_1__initial_release.sql]() name

**Application properties:**

1. zeptolab.server.host: to specify host name of application, for local development will be localhost, for docker
   profile use 0.0.0.0 to enable access via telnet
2. zeptolab.server.port: integer number of application port by default 8080, note: if port changed then need to update docker run command, to expose new changed port.
3. zeptolab.max.channel.users: Property to control number of active users ber channel. Default value 10.
4. zeptolab.channel.message.history: Property to control number of messages to display after joining existing channel.

### Installation

#### Running application in local machine
```shell
cd zeptolab-app
./mvnw clean install
cd zeptolab-server
java -jar zeptolab-app.jar
telnet localhost 8080
```
#### Running application in docker environment

_Note: docker should be installed in running machine :)_

```shell
cd zeptolab-app
docker build -t zeptolab-app .
docker run --name zeptolab-app -dp 8080:8080 zeptolab-app
docker start zeptolab-app
telnet localhost 8080
```

### Application commands.

* /login {username} {password}
* /join {channel name}
* /leave 'No args'
* /list 'No args'
* /users 'No args'
* /send {message}
* /disconnect : 'No args'
* /commands : 'No args, show available commands'

_Example:_
```shell
➜  zeptolab-app git:(devel) ✗ telnet localhost 8080                  
Trying ::1...
telnet: connect to address ::1: Connection refused
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.


/login William 123
User with id: '2fb99f52-a169-4d43-ba79-cab87280d74c' and username: 'William' successfully created

/join programming
Successfully joined to channel: 'programming'

/send Hey there

/leave
You are leaved from channel ...

/commands
Supported commands: 
/login {username} {password}
/join {channel name}
/leave 'No args'
/list 'No args'
/users 'No args'
/send {message}
/disconnect 'No args'
/commands 'No args, show available commands'


/disconnect
Bye!!!
Connection closed by foreign host.
➜  zeptolab-app git:(devel) ✗ 
```


### Application Common components and flow
* _`ZeptolabApplication`_ : Main Application class.
* _`ZeptolabNettyServer`_ : Initialize netty application server.
* _`ZeptolabChannelInitializer`_ : Class to set up channel handlers.
* _`StrDecoder`_ : Decode incoming request in port 8080.
* _`ZeptolabCLIParser`_ : Parsing and validating incoming text requests. After successful parsing `AbstractCommand` object will be created.
* _`AbstractCommand`_ : Generic class of all command requests.
* _`SecurityService`_ : Used for authentication.
* _`ExecutorRegistry`_ : Registry for command executors (each command has their own executor see `AbstractCommandExecutor`).
* etc...

#### Flow

After login user sends text message (command) via telnet.
StrDecoder will handle text request and pass to ZeptolabCLIParser. ZeptolabCLIParser trying to validate request by command type
and creates corresponding command instance (like LoginCommand or SendCommand)
each command object has there command type (CommandType enum).
Command instance passing to ExecutorRegistry execute function.
execute function will identify command executor by there type and pass to execution.

#### Used patterns 

1. Design patterns : Factory method (modified version :) )
2. Architectural patter : Layered architectural pattern.

#### Improvements
Need to improve request execution by adding something like thread pool executor. To execute request independent and without blocking.
Need to review concurrent request execution.
Need to find way to store netty channel information in DB, currently this is stored in simple hash map. (Note: netty channel is not serializable).
Need to have more coverage in unit and integration test, and write new tests without mock-ing.

## From requirements missing multiple device implementation support.
```
Moreover,
the same user can auth twice from different devices, and on both them they should be able to
receive messages.
```