FROM maven:3.8.3-openjdk-17 AS build
COPY . /home/src
WORKDIR /home/src
RUN mvn clean install

FROM openjdk:17.0.1-jdk-slim
COPY --from=build /home/src/zeptolab-server/target/zeptolab-app.jar /home/app/zeptolab-app.jar
EXPOSE 8080:8080
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=docker", "/home/app/zeptolab-app.jar"]