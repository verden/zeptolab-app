package com.zeptolab.services.users.models;


import java.util.UUID;

/**
 * @author William Arustamyan
 */


public final class UserResponse {

  public UUID id;
  public String username;

  public UserResponse(final UUID id, final String username) {
    this.username = username;
    this.id = id;
  }
}
