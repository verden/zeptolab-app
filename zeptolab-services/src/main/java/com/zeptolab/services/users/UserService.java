package com.zeptolab.services.users;


import com.zeptolab.services.users.models.UserRequest;
import com.zeptolab.services.users.models.UserResponse;

import java.util.Optional;

/**
 * @author William Arustamyan
 */


public interface UserService {

  UserResponse add(UserRequest source);

  Optional<UserResponse> find(String username);
}
