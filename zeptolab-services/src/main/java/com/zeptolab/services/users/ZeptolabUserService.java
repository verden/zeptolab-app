package com.zeptolab.services.users;


import com.zeptolab.persistence.entities.User;
import com.zeptolab.persistence.repositories.UserRepository;
import com.zeptolab.services.users.models.UserRequest;
import com.zeptolab.services.users.models.UserResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author William Arustamyan
 */

@Service
public final class ZeptolabUserService implements UserService {

  private static final Logger logger = LoggerFactory.getLogger(UserService.class);

  private final UserRepository repository;

  public ZeptolabUserService(final UserRepository repository) {
    this.repository = repository;
  }

  @Override
  public UserResponse add(final UserRequest source) {
    logger.info("Creating new user with username : {}", source.username);
    return Optional.of(source)
      .map(it -> this.repository.save(new User(it.username, it.password)))
      .map(usr -> new UserResponse(usr.getId(), usr.getUsername()))
      .orElseThrow();
  }

  @Override
  public Optional<UserResponse> find(final String username) {
    logger.info("Trying to find user with username : {}", username);
    return this.repository.findActiveUser(username)
      .map(usr -> new UserResponse(usr.getId(), usr.getUsername()));
  }
}
