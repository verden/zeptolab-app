package com.zeptolab.services.users.models;


/**
 * @author William Arustamyan
 */


public final class UserRequest {

  public String username;
  public String password;

  public UserRequest(final String username, final String password) {
    this.username = username;
    this.password = password;
  }
}
