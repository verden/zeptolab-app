package com.zeptolab.services.session.models;


import java.util.UUID;

/**
 * @author William Arustamyan
 */


public final class UserCtx {

  public final UUID id;
  public final String username;

  public UserCtx(final UUID id, final String username) {
    this.username = username;
    this.id = id;
  }
}
