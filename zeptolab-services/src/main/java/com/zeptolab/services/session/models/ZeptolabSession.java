package com.zeptolab.services.session.models;


import io.netty.channel.ChannelHandlerContext;

import java.util.Optional;

/**
 * @author William Arustamyan
 */


public final class ZeptolabSession {

  public final UserCtx userCtx;
  public final ChannelHandlerContext ctx;
  private ChannelCtx channelCtx;

  public ZeptolabSession(final UserCtx userCtx, final ChannelHandlerContext ctx) {
    this.userCtx = userCtx;
    this.ctx = ctx;
  }

  public void updateChannel(final ChannelCtx channelCtx) {
    this.channelCtx = channelCtx;
  }

  public Optional<ChannelCtx> channelCtx() {
    return Optional.ofNullable(this.channelCtx);
  }

  public boolean hasChannel() {
    return this.channelCtx != null;
  }

  public void dropChannel() {
    this.updateChannel(null);
  }
}
