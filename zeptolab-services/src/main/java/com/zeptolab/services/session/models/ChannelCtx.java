package com.zeptolab.services.session.models;


import java.util.UUID;

/**
 * @author William Arustamyan
 */


public final class ChannelCtx {

  public final UUID id;
  public final String name;


  public ChannelCtx(final UUID id, final String name) {
    this.id = id;
    this.name = name;
  }
}
