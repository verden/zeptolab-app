package com.zeptolab.services.session;


import com.zeptolab.services.session.models.MessageSender;
import com.zeptolab.services.session.models.UserCtx;
import com.zeptolab.services.session.models.ZeptolabSession;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author William Arustamyan
 */


public interface SessionService {

  void store(ChannelHandlerContext ctx, UserCtx userCtx);

  void destroy(UUID userId);

  void release(ZeptolabSession session);

  Optional<ZeptolabSession> lastSession(UUID userId);

  Optional<ZeptolabSession> session(UUID userId);

  List<ZeptolabSession> filterByChannel(UUID channelId);

  boolean canJoin(String channelName);

  void sendMessage(MessageSender sender, UUID toChannel, String message);
}
