package com.zeptolab.services.session;


import com.zeptolab.services.session.models.MessageSender;
import com.zeptolab.services.session.models.UserCtx;
import com.zeptolab.services.session.models.ZeptolabSession;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author William Arustamyan
 */

@Service
public final class ZeptolabSessionService implements SessionService {

  private static final Logger logger = LoggerFactory.getLogger(SessionService.class);

  private final Map<UUID, ZeptolabSession> sessions = new ConcurrentHashMap<>();

  private final Map<UUID, ZeptolabSession> lastConnectedSessions = new ConcurrentHashMap<>();

  private final int channelLimit;

  public ZeptolabSessionService(@Value("${zeptolab.max.channel.users}") final Integer channelLimit) {
    this.channelLimit = channelLimit;
  }

  private static Predicate<ZeptolabSession> filterSubscribers(MessageSender sender, UUID toChannel) {
    return s -> s.channelCtx().isPresent() && s.channelCtx().get().id.equals(toChannel) && !s.userCtx.id.equals(sender.id);
  }

  @Override
  public void store(final ChannelHandlerContext ctx, final UserCtx userCtx) {
    logger.info("Storing session in memory for user with username : {}", userCtx.username);
    this.sessions.put(userCtx.id, new ZeptolabSession(userCtx, ctx));
  }

  @Override
  public void destroy(final UUID userId) {
    logger.info("Destroying session for user with id : {}", userId);
    Optional.ofNullable(this.sessions.get(userId))
      .ifPresent(it -> {
          this.lastConnectedSessions.put(it.userCtx.id, it);
          this.sessions.remove(it.userCtx.id);
        }
      );
  }

  @Override
  public void release(final ZeptolabSession session) {
    logger.info("Release last session for user : {}", session.userCtx.username);
    this.lastConnectedSessions.remove(session.userCtx.id);
    this.sessions.put(session.userCtx.id, session);
  }

  @Override
  public Optional<ZeptolabSession> lastSession(final UUID userId) {
    logger.info("Trying to find last session for user : {}", userId);
    return Optional.ofNullable(this.lastConnectedSessions.get(userId));
  }

  @Override
  public Optional<ZeptolabSession> session(final UUID userId) {
    logger.info("trying to get session for user : {}", userId);
    return Optional.ofNullable(this.sessions.get(userId));
  }

  @Override
  public List<ZeptolabSession> filterByChannel(final UUID channelId) {
    return this.sessions.values().stream()
      .filter(session -> session.channelCtx().isPresent() && session.channelCtx().get().id.equals(channelId))
      .collect(Collectors.toList());
  }

  @Override
  public boolean canJoin(final String channelName) {
    logger.info("Checking user can join to channel : {}", channelName);
    return sessions.values().stream()
      .filter(session -> session.channelCtx().isPresent() && Objects.equals(session.channelCtx().get().name, channelName))
      .count() < this.channelLimit;
  }

  @Override
  public void sendMessage(final MessageSender sender, final UUID toChannel, final String message) {
    logger.info("Sending message to channel : {} from user : {}", toChannel, sender.username);
    sessions.values().stream()
      .filter(filterSubscribers(sender, toChannel))
      .forEach(it -> it.ctx.writeAndFlush("Message : " + sender.username + " : " + message));
  }

}
