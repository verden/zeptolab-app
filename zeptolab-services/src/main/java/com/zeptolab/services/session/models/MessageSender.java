package com.zeptolab.services.session.models;


import java.util.UUID;

/**
 * @author William Arustamyan
 */


public final class MessageSender {

  public final UUID id;
  public final String username;

  public MessageSender(final UUID id, final String username) {
    this.username = username;
    this.id = id;
  }
}
