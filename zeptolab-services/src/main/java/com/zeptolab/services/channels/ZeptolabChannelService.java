package com.zeptolab.services.channels;


import com.zeptolab.persistence.entities.Channel;
import com.zeptolab.persistence.repositories.ChannelRepository;
import com.zeptolab.services.channels.models.ChannelRequest;
import com.zeptolab.services.channels.models.ChannelResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author William Arustamyan
 */

@Service
public class ZeptolabChannelService implements ChannelService {

  private static final Logger logger = LoggerFactory.getLogger(ChannelService.class);

  private final ChannelRepository repository;

  public ZeptolabChannelService(final ChannelRepository repository) {
    this.repository = repository;
  }

  @Override
  public ChannelResponse add(final ChannelRequest source) {
    logger.info("Accepted channel creation request");
    return Optional.of(source)
      .map(it -> this.repository.save(new Channel(it.name)))
      .map(cnl -> new ChannelResponse(cnl.getId(), cnl.getName()))
      .orElseThrow();
  }

  @Override
  public Optional<ChannelResponse> channel(final String name) {
    logger.debug("Try to find channel with name : {}", name);
    return this.repository.findChannel(name)
      .map(c -> new ChannelResponse(c.getId(), c.getName()));
  }

  @Override
  public Set<ChannelResponse> channels() {
    logger.info("Finding all available channels");
    return this.repository.findChannels().stream()
      .map(c -> new ChannelResponse(c.getId(), c.getName()))
      .collect(Collectors.toSet());
  }
}
