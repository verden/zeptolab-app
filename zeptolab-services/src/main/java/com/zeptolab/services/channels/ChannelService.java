package com.zeptolab.services.channels;


import com.zeptolab.services.channels.models.ChannelRequest;
import com.zeptolab.services.channels.models.ChannelResponse;

import java.util.Optional;
import java.util.Set;

/**
 * @author William Arustamyan
 */


public interface ChannelService {

  ChannelResponse add(ChannelRequest source);

  Optional<ChannelResponse> channel(String name);

  Set<ChannelResponse> channels();

}
