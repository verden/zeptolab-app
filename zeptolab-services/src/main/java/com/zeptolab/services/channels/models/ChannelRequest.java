package com.zeptolab.services.channels.models;


/**
 * @author William Arustamyan
 */


public final class ChannelRequest {

  public final String name;

  public ChannelRequest(final String name) {
    this.name = name;
  }
}
