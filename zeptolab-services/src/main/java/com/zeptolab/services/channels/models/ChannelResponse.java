package com.zeptolab.services.channels.models;


import java.util.UUID;

/**
 * @author William Arustamyan
 */


public final class ChannelResponse {

  public final UUID id;
  public final String name;

  public ChannelResponse(final UUID id, final String name) {
    this.id = id;
    this.name = name;
  }
}
