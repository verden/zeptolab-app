package com.zeptolab.services.executors;


import com.zeptolab.services.channels.ChannelService;
import com.zeptolab.services.channels.models.ChannelResponse;
import com.zeptolab.services.cli.commands.CommandType;
import com.zeptolab.services.cli.commands.ListCommand;
import com.zeptolab.services.executors.responses.Void;
import com.zeptolab.services.session.SessionService;
import com.zeptolab.services.session.models.ZeptolabSession;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Set;

/**
 * @author William Arustamyan
 */


@CommandExecutor(type = CommandType.LIST)
public final class ListExecutor implements AbstractCommandExecutor<ListCommand, Void> {

  private static final Logger logger = LoggerFactory.getLogger(ListExecutor.class);

  private final SessionService sessionService;
  private final ChannelService channelService;

  public ListExecutor(final SessionService sessionService,
                      final ChannelService channelService) {
    this.sessionService = sessionService;
    this.channelService = channelService;
  }

  @Override
  public Void execute(final ChannelHandlerContext ctx, final ListCommand command) {
    logger.info("Start executing list command");
    final Optional<ZeptolabSession> found = this.sessionService.session(command.userId());

    if (found.isEmpty()) {
      logger.warn("Session is not found for list command");
      ctx.writeAndFlush(String.format("Session not found for user with id: {%s}", command.userId().toString()));
      ctx.close();
      return Void.VOID;
    }

    final Set<ChannelResponse> channels = this.channelService.channels();
    logger.debug("Found active channels with size {}", channels.size());
    if (channels.isEmpty()) {
      ctx.writeAndFlush("There is no channels available: You can be first !");
    } else {
      ctx.writeAndFlush(this.buildTextResponse(channels));
    }

    return Void.VOID;
  }

  private String buildTextResponse(final Set<ChannelResponse> channels) {
    final StringBuilder sb = new StringBuilder(
      "List of available channels: \nid                                   : name\n"
    );
    channels.forEach(c -> sb.append(c.id).append(" : ").append(c.name).append("\n"));
    return sb.toString();
  }
}
