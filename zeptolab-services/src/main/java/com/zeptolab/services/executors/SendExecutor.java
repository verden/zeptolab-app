package com.zeptolab.services.executors;


import com.zeptolab.services.cli.commands.CommandType;
import com.zeptolab.services.cli.commands.SendCommand;
import com.zeptolab.services.executors.responses.Void;
import com.zeptolab.services.messages.MessageService;
import com.zeptolab.services.messages.models.MessageRequest;
import com.zeptolab.services.messages.models.MessageResponse;
import com.zeptolab.services.session.SessionService;
import com.zeptolab.services.session.models.MessageSender;
import com.zeptolab.services.session.models.ZeptolabSession;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * @author William Arustamyan
 */


@CommandExecutor(type = CommandType.SEND)
public final class SendExecutor implements AbstractCommandExecutor<SendCommand, Void> {

  private static final Logger logger = LoggerFactory.getLogger(SendExecutor.class);

  private final MessageService messageService;
  private final SessionService sessionService;

  public SendExecutor(final MessageService messageService, final SessionService sessionService) {
    this.messageService = messageService;
    this.sessionService = sessionService;
  }


  @Override
  public Void execute(final ChannelHandlerContext ctx, final SendCommand command) {
    logger.info("Start executing send command");
    final Optional<ZeptolabSession> found = this.sessionService.session(command.userId());

    if (found.isEmpty()) {
      logger.warn("Session is not found for send command");
      ctx.writeAndFlush(String.format("Session not found for user with id: {%s}", command.userId().toString()));
      ctx.close();
      return Void.VOID;
    }

    final ZeptolabSession session = found.get();

    if (session.channelCtx().isEmpty()) {
      logger.warn("User has no any active channel");
      ctx.writeAndFlush("You need to join channel to send message");
      return Void.VOID;
    }

    final MessageResponse response = this.messageService.add(
      new MessageRequest(session.userCtx.id, session.channelCtx().get().id, command.message)
    );
    logger.debug("User message successfully created with id : {}", response.getId());
    this.sessionService.sendMessage(
      new MessageSender(response.getUser().id, response.getUser().username),
      session.channelCtx().get().id,
      response.getContent()
    );

    return Void.VOID;
  }
}
