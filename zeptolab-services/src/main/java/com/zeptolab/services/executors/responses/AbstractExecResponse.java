package com.zeptolab.services.executors.responses;


import com.zeptolab.services.cli.Typed;
import com.zeptolab.services.cli.commands.CommandType;

/**
 * @author William Arustamyan
 */


public abstract class AbstractExecResponse extends Typed {

  public AbstractExecResponse(final CommandType type) {
    super(type);
  }
}
