package com.zeptolab.services.executors;


import com.zeptolab.services.channels.ChannelService;
import com.zeptolab.services.channels.models.ChannelRequest;
import com.zeptolab.services.channels.models.ChannelResponse;
import com.zeptolab.services.cli.commands.CommandType;
import com.zeptolab.services.cli.commands.JoinCommand;
import com.zeptolab.services.executors.messagehelper.TxtMessageBuilder;
import com.zeptolab.services.executors.responses.Void;
import com.zeptolab.services.messages.MessageService;
import com.zeptolab.services.messages.models.MessageResponse;
import com.zeptolab.services.session.SessionService;
import com.zeptolab.services.session.models.ChannelCtx;
import com.zeptolab.services.session.models.ZeptolabSession;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.util.Optional;
import java.util.Set;

/**
 * @author William Arustamyan
 */


@CommandExecutor(type = CommandType.JOIN)
public final class JoinExecutor implements AbstractCommandExecutor<JoinCommand, Void> {

  private static final Logger logger = LoggerFactory.getLogger(JoinExecutor.class);

  private final MessageService messageService;
  private final SessionService sessionService;
  private final ChannelService channelService;

  private final int channelHistorySize;

  public JoinExecutor(@Value("${zeptolab.channel.message.history}") final Integer historySize,
                      final SessionService sessionService,
                      final ChannelService channelService,
                      final MessageService messageService) {
    this.sessionService = sessionService;
    this.channelService = channelService;
    this.messageService = messageService;
    this.channelHistorySize = historySize;
  }

  @Override
  public Void execute(final ChannelHandlerContext ctx, final JoinCommand command) {
    logger.info("Start executing join command");
    final Optional<ZeptolabSession> found = this.sessionService.session(command.userId());

    if (found.isEmpty()) {
      logger.warn("Session is not found for join command");
      ctx.writeAndFlush(String.format("Session not found for user with id: {%s}", command.userId().toString()));
      ctx.close();
      return Void.VOID;
    }

    if (!this.sessionService.canJoin(command.channelName)) {
      logger.warn("Unable to join channel, Channel is full");
      ctx.writeAndFlush("Channel users limit exceeded. There is no space ...");
      return Void.VOID;
    }

    final ChannelResponse response = this.channelService.channel(command.channelName)
      .orElseGet(() -> channelService.add(new ChannelRequest(command.channelName)));

    found.get().updateChannel(new ChannelCtx(response.id, response.name));

    final Set<MessageResponse> lastMessages = this.messageService.findLastMessages(response.id, this.channelHistorySize);

    logger.debug("Found last messages with count {}", lastMessages.size());
    ctx.writeAndFlush(TxtMessageBuilder.buildLastMessages(response.name, command.userId(), lastMessages));

    return Void.VOID;
  }
}
