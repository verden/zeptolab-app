package com.zeptolab.services.executors;


import com.zeptolab.services.cli.commands.AbstractCommand;
import com.zeptolab.services.executors.responses.AbstractExecResponse;
import io.netty.channel.ChannelHandlerContext;

/**
 * @author William Arustamyan
 */


public interface AbstractCommandExecutor<Command extends AbstractCommand, Response extends AbstractExecResponse> {

  Response execute(ChannelHandlerContext ctx, Command command);
}
