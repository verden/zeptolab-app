package com.zeptolab.services.executors.responses;


import com.zeptolab.services.cli.commands.CommandType;

/**
 * @author William Arustamyan
 */


public final class Void extends AbstractExecResponse {

  public static final Void VOID = new Void();

  private Void() {
    super(CommandType.EMPTY);
  }
}
