package com.zeptolab.services.executors;


import com.zeptolab.services.cli.commands.CommandType;
import com.zeptolab.services.cli.commands.UsersCommand;
import com.zeptolab.services.executors.responses.Void;
import com.zeptolab.services.session.SessionService;
import com.zeptolab.services.session.models.ChannelCtx;
import com.zeptolab.services.session.models.ZeptolabSession;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.UUID;

/**
 * @author William Arustamyan
 */


@CommandExecutor(type = CommandType.USERS)
public final class UsersExecutor implements AbstractCommandExecutor<UsersCommand, Void> {

  private static final Logger logger = LoggerFactory.getLogger(UsersExecutor.class);

  private final SessionService sessionService;

  public UsersExecutor(final SessionService sessionService) {
    this.sessionService = sessionService;
  }

  @Override
  public Void execute(final ChannelHandlerContext ctx, final UsersCommand command) {
    logger.info("Start executing users command");
    final Optional<ZeptolabSession> found = this.sessionService.session(command.userId());

    if (found.isEmpty()) {
      logger.warn("Session is not found for users command");
      ctx.writeAndFlush(String.format("Session not found for user with id: {%s}", command.userId().toString()));
      return Void.VOID;
    }

    final Optional<ChannelCtx> opChannelCtx = found.get().channelCtx();

    if (opChannelCtx.isEmpty()) {
      logger.warn("User has no aby active channel");
      ctx.writeAndFlush("You are not joined to any channel...");
      return Void.VOID;
    }

    ctx.writeAndFlush(this.buildTxtResponse(command.userId(), opChannelCtx.get()));
    return Void.VOID;
  }

  private String buildTxtResponse(final UUID currentUserId, final ChannelCtx source) {
    final StringBuilder sb = new StringBuilder(
      String.format("List of available users in channel: '%s'\n", source.name)
    );

    this.sessionService.filterByChannel(source.id).forEach(session -> {
      if (session.userCtx.id.equals(currentUserId)) {
        sb.append(session.userCtx.username).append(" -> ").append("You\n");
      } else {
        sb.append(session.userCtx.username).append("\n");
      }
    });

    return sb.toString();
  }
}
