package com.zeptolab.services.executors;


import com.zeptolab.services.cli.commands.CommandType;
import com.zeptolab.services.cli.commands.LeaveCommand;
import com.zeptolab.services.executors.responses.Void;
import com.zeptolab.services.session.SessionService;
import com.zeptolab.services.session.models.ZeptolabSession;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * @author William Arustamyan
 */


@CommandExecutor(type = CommandType.LEAVE)
public final class LeaveExecutor implements AbstractCommandExecutor<LeaveCommand, Void> {

  private static final Logger logger = LoggerFactory.getLogger(LeaveExecutor.class);

  private final SessionService sessionService;

  public LeaveExecutor(final SessionService sessionService) {
    this.sessionService = sessionService;
  }

  @Override
  public Void execute(final ChannelHandlerContext ctx, final LeaveCommand command) {
    logger.info("Start executing leave command");
    final Optional<ZeptolabSession> found = this.sessionService.session(command.userId());

    if (found.isEmpty()) {
      logger.warn("Session is not found for leave command");
      ctx.writeAndFlush(String.format("Session not found for user with id: {%s}", command.userId().toString()));
      return Void.VOID;
    }

    final ZeptolabSession session = found.get();

    if (session.channelCtx().isEmpty()) {
      ctx.writeAndFlush("You are not joined to any channel ...");
      return Void.VOID;
    }

    session.dropChannel();

    ctx.writeAndFlush("You are leaved from channel ...");

    return Void.VOID;
  }
}
