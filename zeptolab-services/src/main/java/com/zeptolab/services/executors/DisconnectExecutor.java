package com.zeptolab.services.executors;


import com.zeptolab.services.cli.commands.CommandType;
import com.zeptolab.services.cli.commands.DisconnectCommand;
import com.zeptolab.services.executors.responses.Void;
import com.zeptolab.services.session.SessionService;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author William Arustamyan
 */


@CommandExecutor(type = CommandType.DISCONNECT)
public final class DisconnectExecutor implements AbstractCommandExecutor<DisconnectCommand, Void> {

  private static final Logger logger = LoggerFactory.getLogger(DisconnectExecutor.class);

  private final SessionService sessionService;

  public DisconnectExecutor(final SessionService sessionService) {
    this.sessionService = sessionService;
  }

  @Override
  public Void execute(final ChannelHandlerContext ctx, final DisconnectCommand command) {
    logger.info("Start executing disconnect command");
    this.sessionService.destroy(command.userId());
    ctx.writeAndFlush("Bye!!!");
    ctx.close();
    ctx.disconnect();
    return Void.VOID;
  }
}
