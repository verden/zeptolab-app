package com.zeptolab.services.executors;


import com.zeptolab.services.cli.commands.AbstractCommand;
import com.zeptolab.services.cli.commands.CommandType;
import com.zeptolab.services.executors.responses.AbstractExecResponse;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author William Arustamyan
 */


@Component
@SuppressWarnings("unchecked")
public final class ExecutorRegistry {

  private final Map<CommandType, AbstractCommandExecutor<AbstractCommand, AbstractExecResponse>> executorsRegistry = new EnumMap<>(CommandType.class);


  public ExecutorRegistry(List<? extends AbstractCommandExecutor<? extends AbstractCommand, ? extends AbstractExecResponse>> executorBeans) {
    executorBeans.forEach(
      it -> this._findAnnotation(it)
        .map(ano -> this.executorsRegistry.put(ano.type(), (AbstractCommandExecutor<AbstractCommand, AbstractExecResponse>) it))
    );
  }

  public <Command extends AbstractCommand, R extends AbstractExecResponse> R execute(final ChannelHandlerContext ctx, final Command command) {
    return (R) this.executorsRegistry.get(command.type()).execute(ctx, command);
  }

  private Optional<CommandExecutor> _findAnnotation(AbstractCommandExecutor<? extends AbstractCommand, ? extends AbstractExecResponse> executor) {
    return Optional.ofNullable(
      AnnotationUtils.findAnnotation(executor.getClass(), CommandExecutor.class)
    );
  }
}
