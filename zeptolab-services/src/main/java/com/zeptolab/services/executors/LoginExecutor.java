package com.zeptolab.services.executors;


import com.zeptolab.services.cli.commands.CommandType;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.executors.messagehelper.TxtMessageBuilder;
import com.zeptolab.services.executors.responses.LoginResponse;
import com.zeptolab.services.messages.MessageService;
import com.zeptolab.services.session.SessionService;
import com.zeptolab.services.session.models.ChannelCtx;
import com.zeptolab.services.session.models.UserCtx;
import com.zeptolab.services.session.models.ZeptolabSession;
import com.zeptolab.services.users.UserService;
import com.zeptolab.services.users.models.UserRequest;
import com.zeptolab.services.users.models.UserResponse;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.util.Optional;

/**
 * @author William Arustamyan
 */

@CommandExecutor(type = CommandType.LOGIN)
public final class LoginExecutor implements AbstractCommandExecutor<LoginCommand, LoginResponse> {

  private static final Logger logger = LoggerFactory.getLogger(LoginExecutor.class);

  private final UserService userService;
  private final MessageService messageService;
  private final SessionService sessionService;

  private final int channelHistorySize;

  public LoginExecutor(@Value("${zeptolab.channel.message.history}") final Integer historySize,
                       final UserService userService, final SessionService sessionService,
                       final MessageService messageService) {
    this.channelHistorySize = historySize;
    this.sessionService = sessionService;
    this.messageService = messageService;
    this.userService = userService;
  }

  @Override
  public LoginResponse execute(final ChannelHandlerContext ctx, final LoginCommand command) {
    logger.info("Start executing login command");
    final Optional<UserResponse> foundUser = this.userService.find(command.username);
    //sorry for this, we need to identify is new user created to sand back valid message
    final UserResponse ur;
    final boolean isNewUser;
    String lastMessages = null;

    if (foundUser.isPresent()) {
      isNewUser = false;
      ur = foundUser.get();
      logger.info("User is found with username : {}", ur.username);
    } else {
      isNewUser = true;
      ur = this.userService.add(new UserRequest(command.username, command.password));
      logger.info("User not found. Created new user with username : {}", ur.username);
    }
    logger.debug("Try to find last connected session");
    final Optional<ZeptolabSession> opSession = this.sessionService.lastSession(ur.id);
    //try to recover last session
    if (opSession.isPresent()) {
      logger.debug("User has last session");
      //user has last session
      final ZeptolabSession session = opSession.get();
      if (session.channelCtx().isPresent()) {
        logger.debug("Trying to recover user last session with channel");
        //user disconnects without leaving channel
        final ChannelCtx channelCtx = session.channelCtx().get();
        final ZeptolabSession newSession = new ZeptolabSession(new UserCtx(ur.id, ur.username), ctx);
        //check channel has space to join
        if (this.sessionService.canJoin(channelCtx.name)) {
          logger.debug("User can join previews channel");
          newSession.updateChannel(channelCtx);
          lastMessages = TxtMessageBuilder.buildLastMessages(channelCtx.name, ur.id, this.messageService.findLastMessages(channelCtx.id, this.channelHistorySize));
        }
        //release last session remove from last connected session list
        this.sessionService.release(newSession);
      }
    } else {
      //new user creation
      logger.debug("Creating new user session");
      this.sessionService.store(ctx, new UserCtx(ur.id, ur.username));
    }
    return new LoginResponse(ur.id, ur.username, isNewUser, lastMessages);
  }
}
