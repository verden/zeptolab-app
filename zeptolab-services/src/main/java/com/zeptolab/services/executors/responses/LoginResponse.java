package com.zeptolab.services.executors.responses;


import com.zeptolab.services.cli.commands.CommandType;

import java.util.UUID;

/**
 * @author William Arustamyan
 */


public final class LoginResponse extends AbstractExecResponse {

  public final UUID userId;
  public final String username;
  public final boolean isNewUser;
  public final String lastMessages;

  public LoginResponse(final UUID userId, final String username, boolean isNewUser, final String lastMessages) {
    super(CommandType.LOGIN);
    this.userId = userId;
    this.username = username;
    this.isNewUser = isNewUser;
    this.lastMessages = lastMessages;
  }
}
