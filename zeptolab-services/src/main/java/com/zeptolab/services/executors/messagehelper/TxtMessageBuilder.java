package com.zeptolab.services.executors.messagehelper;


import com.zeptolab.services.messages.models.MessageResponse;

import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.UUID;

/**
 * @author William Arustamyan
 */


public final class TxtMessageBuilder {

  private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  private TxtMessageBuilder() {
    throw new AssertionError("static only");
  }

  public static String buildLastMessages(final String channelName, final UUID ignoredUserId, final Set<MessageResponse> messages) {
    final StringBuilder sb = new StringBuilder(String.format("Successfully joined to channel: '%s'", channelName));
    if (messages.isEmpty()) {
      return sb.toString();
    }
    sb.append("\n").append("Last Messages : \n");
    messages.forEach(msg -> {
        if (msg.getUser().id.equals(ignoredUserId)) return;
        sb.append(msg.getCreated().format(dtf))
          .append("  ")
          .append(msg.getUser().username)
          .append(" : ")
          .append(msg.getContent())
          .append("\n");
      }
    );
    return sb.toString();
  }
}
