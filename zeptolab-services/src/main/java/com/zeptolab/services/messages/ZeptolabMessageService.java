package com.zeptolab.services.messages;


import com.zeptolab.persistence.entities.Message;
import com.zeptolab.persistence.repositories.ChannelRepository;
import com.zeptolab.persistence.repositories.MessageRepository;
import com.zeptolab.persistence.repositories.UserRepository;
import com.zeptolab.services.channels.models.ChannelResponse;
import com.zeptolab.services.messages.models.MessageRequest;
import com.zeptolab.services.messages.models.MessageResponse;
import com.zeptolab.services.users.models.UserResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

@Service
public class ZeptolabMessageService implements MessageService {

  private static final Logger logger = LoggerFactory.getLogger(MessageService.class);

  private final MessageRepository repository;
  private final UserRepository userRepository;
  private final ChannelRepository channelRepository;

  public ZeptolabMessageService(final MessageRepository repository,
                                final UserRepository userRepository,
                                final ChannelRepository channelRepository) {
    this.channelRepository = channelRepository;
    this.userRepository = userRepository;
    this.repository = repository;
  }

  @Override
  @Transactional
  public MessageResponse add(final MessageRequest source) {
    logger.info("Start creating new message");
    return Optional.of(source)
      .map(this::convert)
      .map(this.repository::save)
      .map(this::toResponse)
      .orElseThrow();
  }

  @Override
  @Transactional(readOnly = true)
  public Set<MessageResponse> findLastMessages(final UUID channelId, int count) {
    logger.info("Query last {} messages in channel : {} ", count, channelId);
    final List<MessageResponse> messages = this.repository.findAllByChannel_Id(
        channelId, PageRequest.of(0, count, Sort.Direction.DESC, "created")
      ).stream()
      .map(this::toResponse)
      .toList();

    final Set<MessageResponse> orderedMessages = new TreeSet<>(Comparator.comparing(MessageResponse::getCreated));
    orderedMessages.addAll(messages);
    logger.debug("Found messages with size : {}", orderedMessages.size());
    return orderedMessages;
  }

  private Message convert(final MessageRequest source) {
    return new Message(
      source.content,
      this.channelRepository.findById(source.channelId).orElseThrow(),
      this.userRepository.findById(source.userId).orElseThrow()
    );
  }

  private MessageResponse toResponse(final Message message) {
    final MessageResponse response = new MessageResponse();
    response.setId(message.getId());
    response.setUser(new UserResponse(message.getUser().getId(), message.getUser().getUsername()));
    response.setChannel(new ChannelResponse(message.getChannel().getId(), message.getChannel().getName()));
    response.setContent(message.getContent());
    response.setCreated(message.getCreated());
    return response;
  }
}
