package com.zeptolab.services.messages.models;


import com.zeptolab.services.channels.models.ChannelResponse;
import com.zeptolab.services.users.models.UserResponse;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

@Data
public final class MessageResponse {

  private UUID id;

  private UserResponse user;

  private ChannelResponse channel;

  private String content;

  private LocalDateTime created;

}
