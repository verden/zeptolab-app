package com.zeptolab.services.messages.models;


import java.util.UUID;

/**
 * @author William Arustamyan
 */


public final class MessageRequest {

  public final UUID userId;
  public final UUID channelId;
  public final String content;

  public MessageRequest(final UUID userId, final UUID channelId, final String content) {
    this.userId = userId;
    this.channelId = channelId;
    this.content = content;
  }
}
