package com.zeptolab.services.messages;


import com.zeptolab.services.messages.models.MessageRequest;
import com.zeptolab.services.messages.models.MessageResponse;

import java.util.Set;
import java.util.UUID;

/**
 * @author William Arustamyan
 */


public interface MessageService {

  MessageResponse add(MessageRequest source);

  Set<MessageResponse> findLastMessages(UUID channelId, int count);
}
