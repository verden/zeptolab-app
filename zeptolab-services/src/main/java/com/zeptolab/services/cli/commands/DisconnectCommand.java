package com.zeptolab.services.cli.commands;


import java.util.UUID;

/**
 * @author William Arustamyan
 */


public final class DisconnectCommand extends UserCommand {

  public DisconnectCommand(final UUID userId) {
    super(userId, CommandType.DISCONNECT);
  }

  public DisconnectCommand() {
    super(null, CommandType.DISCONNECT);
  }
}
