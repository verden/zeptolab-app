package com.zeptolab.services.cli.commands;


/**
 * @author William Arustamyan
 */


public final class SendCommand extends UserCommand {

  public final String message;

  public SendCommand(final String message) {
    super(null, CommandType.SEND);
    this.message = message;
  }
}
