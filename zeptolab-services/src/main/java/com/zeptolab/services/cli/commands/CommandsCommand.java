package com.zeptolab.services.cli.commands;


/**
 * @author William Arustamyan
 */


public final class CommandsCommand extends AbstractCommand {

  public CommandsCommand() {
    super(CommandType.COMMANDS);
  }
}
