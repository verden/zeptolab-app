package com.zeptolab.services.cli.commands;


import java.util.Arrays;
import java.util.Optional;

/**
 * @author William Arustamyan
 */


public enum CommandType {

  LOGIN("/login", "/login {username} {password}"),
  JOIN("/join", "/join {channel name}"),
  LEAVE("/leave", "/leave 'No args'"),
  LIST("/list", "/list 'No args'"),
  USERS("/users", "/users 'No args'"),
  SEND("/send", "/send {message}"),
  DISCONNECT("/disconnect", "/disconnect 'No args'"),

  COMMANDS("/commands", "/commands 'No args, show available commands'"),
  EMPTY("", "");


  private final String txtValue;
  private final String pattern;

  CommandType(final String txtValue, final String pattern) {
    this.txtValue = txtValue;
    this.pattern = pattern;
  }

  public static Optional<CommandType> from(final String input) {
    return Arrays.stream(values())
      .filter(it -> it.txtValue.equals(input))
      .findFirst();
  }

  public static String commandGuide() {
    final StringBuilder sb = new StringBuilder("Supported commands: \n");
    Arrays.stream(values()).forEach(it -> sb.append(it.pattern).append("\n"));
    return sb.toString();
  }

  public String txtValue() {
    return this.txtValue;
  }

  public String pattern() {
    return this.pattern;
  }
}
