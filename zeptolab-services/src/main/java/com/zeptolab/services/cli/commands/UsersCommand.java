package com.zeptolab.services.cli.commands;


/**
 * @author William Arustamyan
 */


public final class UsersCommand extends UserCommand {

  public UsersCommand() {
    super(null, CommandType.USERS);
  }
}
