package com.zeptolab.services.cli.commands;


/**
 * @author William Arustamyan
 */


public final class LoginCommand extends AbstractCommand {

  public final String username;
  public final String password;

  public LoginCommand(final String username, final String password) {
    super(CommandType.LOGIN);
    this.username = username;
    this.password = password;
  }
}
