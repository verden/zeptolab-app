package com.zeptolab.services.cli.commands;


/**
 * @author William Arustamyan
 */


public final class JoinCommand extends UserCommand {

  public final String channelName;

  public JoinCommand(final String channelName) {
    super(null, CommandType.JOIN);
    this.channelName = channelName;
  }
}
