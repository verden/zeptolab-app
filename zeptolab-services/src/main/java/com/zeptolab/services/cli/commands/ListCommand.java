package com.zeptolab.services.cli.commands;


/**
 * @author William Arustamyan
 */


public final class ListCommand extends UserCommand {

  public ListCommand() {
    super(null, CommandType.LIST);
  }
}
