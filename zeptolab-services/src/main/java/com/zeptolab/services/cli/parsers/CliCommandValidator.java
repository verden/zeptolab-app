package com.zeptolab.services.cli.parsers;


import com.zeptolab.services.cli.commands.CommandType;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Function;

/**
 * @author William Arustamyan
 */


public final class CliCommandValidator {

  private static final Logger logger = LoggerFactory.getLogger(CliCommandValidator.class);

  private final Map<CommandType, Function<String[], Boolean>> validators = new EnumMap<>(CommandType.class);

  public CliCommandValidator() {
    this._initValidators();
  }

  public boolean isValidCommand(final CommandType commandType, final String[] args) {
    logger.info("Start validating user command {}", commandType);
    return this.validators.get(commandType).apply(args);
  }

  private void _initValidators() {
    this.validators.put(CommandType.LOGIN, this.tripleArgsValidator());
    this.validators.put(CommandType.JOIN, this.doubleArgsValidator());
    this.validators.put(CommandType.LEAVE, this.singleArgsValidator());
    this.validators.put(CommandType.DISCONNECT, this.singleArgsValidator());
    this.validators.put(CommandType.LIST, this.singleArgsValidator());
    this.validators.put(CommandType.USERS, this.singleArgsValidator());
    this.validators.put(CommandType.SEND, this.multipleArgsValidator());
    this.validators.put(CommandType.COMMANDS, singleArgsValidator());
  }


  private Function<String[], Boolean> singleArgsValidator() {
    return args -> args.length == 1;
  }

  private Function<String[], Boolean> doubleArgsValidator() {
    return args -> {
      if (args.length != 2) {
        return false;
      }
      return !StringUtils.isBlank(args[1]);
    };
  }

  private Function<String[], Boolean> tripleArgsValidator() {
    return args -> {
      if (args.length != 3) {
        return false;
      }
      return !StringUtils.isBlank(args[1]) && !StringUtils.isBlank(args[2]);
    };
  }

  private Function<String[], Boolean> multipleArgsValidator() {
    return args -> {
      if (args.length < 2) {
        return false;
      }
      return !StringUtils.isBlank(args[1]);
    };
  }
}
