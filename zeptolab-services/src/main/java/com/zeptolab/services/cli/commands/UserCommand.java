package com.zeptolab.services.cli.commands;


import java.util.UUID;

/**
 * @author William Arustamyan
 */


public abstract class UserCommand extends AbstractCommand {

  protected UUID userId;

  public UserCommand(final UUID userId, final CommandType type) {
    super(type);
    this.userId = userId;
  }

  public void addUserId(final UUID userId) {
    this.userId = userId;
  }

  public UUID userId() {
    return this.userId;
  }
}
