package com.zeptolab.services.cli.commands;


/**
 * @author William Arustamyan
 */


public final class LeaveCommand extends UserCommand {

  public LeaveCommand() {
    super(null, CommandType.LEAVE);
  }
}
