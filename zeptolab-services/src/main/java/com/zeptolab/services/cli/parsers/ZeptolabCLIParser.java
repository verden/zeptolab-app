package com.zeptolab.services.cli.parsers;


import com.zeptolab.services.cli.commands.AbstractCommand;
import com.zeptolab.services.cli.commands.CommandType;
import com.zeptolab.services.cli.commands.CommandsCommand;
import com.zeptolab.services.cli.commands.DisconnectCommand;
import com.zeptolab.services.cli.commands.JoinCommand;
import com.zeptolab.services.cli.commands.LeaveCommand;
import com.zeptolab.services.cli.commands.ListCommand;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.cli.commands.SendCommand;
import com.zeptolab.services.cli.commands.UsersCommand;
import com.zeptolab.services.exceptions.InvalidCommandException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author William Arustamyan
 */


public final class ZeptolabCLIParser {

  private static final Logger logger = LoggerFactory.getLogger(ZeptolabCLIParser.class);

  private static final String err_command_args = "Invalid command line arguments ... command: {%s} supports pattern: {%s}";
  private static final String err_unknown_cmd = "Unknown command: {%s}";

  private final CliCommandValidator commandValidator;

  public ZeptolabCLIParser() {
    this.commandValidator = new CliCommandValidator();
  }

  public AbstractCommand parse(final String content) throws InvalidCommandException {
    final String[] args = content.split(" ");
    final CommandType commandType = this.identifyCommand(args[0]);

    if (!this.commandValidator.isValidCommand(commandType, args)) {
      logger.error("Invalid command line arguments: {}", Arrays.toString(args));
      throw new InvalidCommandException(String.format(err_command_args, commandType.txtValue(), commandType.pattern()));
    }

    return this.buildCommandInstance(commandType, args);
  }

  private CommandType identifyCommand(final String strCommand) throws InvalidCommandException {
    return CommandType.from(strCommand)
      .orElseThrow(() -> new InvalidCommandException(String.format(err_unknown_cmd, strCommand)));
  }

  private AbstractCommand buildCommandInstance(final CommandType type, final String[] args) {
    return switch (type) {
      case LOGIN -> new LoginCommand(args[1], args[2]);
      case JOIN -> new JoinCommand(args[1]);
      case LEAVE -> new LeaveCommand();
      case LIST -> new ListCommand();
      case USERS -> new UsersCommand();
      case SEND -> new SendCommand(this.concatMessage(args));
      case DISCONNECT -> new DisconnectCommand();
      case COMMANDS -> new CommandsCommand();
      default -> new AbstractCommand(CommandType.EMPTY) {
      };
    };
  }

  private String concatMessage(final String[] args) {
    return Arrays.stream(args)
      .skip(1)
      .collect(Collectors.joining(" "));
  }
}
