package com.zeptolab.services.cli.commands;


import com.zeptolab.services.cli.Typed;

/**
 * @author William Arustamyan
 */


public abstract class AbstractCommand extends Typed {

  public AbstractCommand(final CommandType type) {
    super(type);
  }

}
