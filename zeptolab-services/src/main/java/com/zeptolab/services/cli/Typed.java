package com.zeptolab.services.cli;


import com.zeptolab.services.cli.commands.CommandType;

/**
 * @author William Arustamyan
 */


public abstract class Typed {

  protected CommandType type;

  public Typed(final CommandType type) {
    this.type = type;
  }

  public CommandType type() {
    return this.type;
  }
}
