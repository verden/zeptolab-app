package com.zeptolab.services.auth;


import com.zeptolab.persistence.entities.User;
import com.zeptolab.persistence.repositories.UserRepository;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.exceptions.AuthException;
import com.zeptolab.services.exceptions.InvalidCredentialsException;
import com.zeptolab.services.exceptions.UserActiveSessionException;
import com.zeptolab.services.exceptions.UserNotFoundException;
import com.zeptolab.services.session.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author William Arustamyan
 */

@Service
public final class ZeptolabSecurityService implements SecurityService {

  private static final Logger logger = LoggerFactory.getLogger(SecurityService.class);

  private final UserRepository repository;
  private final SessionService sessionService;

  public ZeptolabSecurityService(final UserRepository repository, final SessionService sessionService) {
    this.sessionService = sessionService;
    this.repository = repository;
  }

  @Override
  public void authenticate(final LoginCommand loginCommand) throws AuthException {
    logger.info("Start authenticate login request");

    final User user = this.repository.findActiveUser(loginCommand.username)
      .orElseThrow(UserNotFoundException::new);

    logger.debug("Found user with username: {}", user.getUsername());

    if (!user.getPassword().equals(loginCommand.password)) {
      throw new InvalidCredentialsException();
    }

    if (this.sessionService.session(user.getId()).isPresent()) {
      throw new UserActiveSessionException();
    }
  }
}
