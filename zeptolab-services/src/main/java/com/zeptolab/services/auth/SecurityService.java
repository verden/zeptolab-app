package com.zeptolab.services.auth;


import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.exceptions.AuthException;

/**
 * @author William Arustamyan
 */


public interface SecurityService {

  void authenticate(final LoginCommand loginCommand) throws AuthException;
}
