package com.zeptolab.services.exceptions;


/**
 * @author William Arustamyan
 */


public final class UserNotFoundException extends AuthException {

  private static final String message = "User with given username and password not found";

  @Override
  public String getMessage() {
    return message;
  }
}
