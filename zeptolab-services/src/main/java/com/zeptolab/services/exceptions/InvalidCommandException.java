package com.zeptolab.services.exceptions;


/**
 * @author William Arustamyan
 */


public final class InvalidCommandException extends Exception {

  private final String message;

  public InvalidCommandException(final String message) {
    this.message = message;
  }

  public String getMessage() {
    return this.message;
  }
}
