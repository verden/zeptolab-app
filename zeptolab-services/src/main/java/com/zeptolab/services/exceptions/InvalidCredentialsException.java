package com.zeptolab.services.exceptions;


/**
 * @author William Arustamyan
 */


public final class InvalidCredentialsException extends AuthException {

  private static final String message = "Username or password is incorrect";

  public String getMessage() {
    return message;
  }
}
