package com.zeptolab.services.exceptions;


/**
 * @author William Arustamyan
 */


public final class UserActiveSessionException extends AuthException {

  private static String message = "Given user already connected";

  @Override
  public String getMessage() {
    return message;
  }
}
