package com.zeptolab.session;


import com.zeptolab.services.session.SessionService;
import com.zeptolab.services.session.models.ChannelCtx;
import com.zeptolab.services.session.models.UserCtx;
import com.zeptolab.services.session.models.ZeptolabSession;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author William Arustamyan
 */


@SpringBootTest
public class SessionServiceIntegrationTest {

  @Autowired
  private SessionService sessionService;

  private final ChannelHandlerContext ctx = Mockito.mock(ChannelHandlerContext.class);

  //this test covers also find in session case
  @Test
  public void test_store_in_session_OK() {
    final UUID userId = UUID.randomUUID();
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserCtx userCtx = new UserCtx(userId, username);
    this.sessionService.store(this.ctx, userCtx);

    final Optional<ZeptolabSession> found = this.sessionService.session(userId);

    assertTrue(found.isPresent());

    final ZeptolabSession session = found.get();

    assertEquals(userCtx.id, session.userCtx.id);
    assertEquals(userCtx.username, session.userCtx.username);
  }

  @Test
  public void test_find_user_session_FAIL() {
    final UUID userId = UUID.randomUUID();
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserCtx userCtx = new UserCtx(userId, username);
    this.sessionService.store(this.ctx, userCtx);

    final Optional<ZeptolabSession> found = this.sessionService.session(UUID.randomUUID());

    assertTrue(found.isEmpty());
  }

  @Test
  public void test_destroy_session_OK() {
    final UUID userId = UUID.randomUUID();
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserCtx userCtx = new UserCtx(userId, username);
    this.sessionService.store(this.ctx, userCtx);

    this.sessionService.destroy(userId);

    assertTrue(this.sessionService.session(UUID.randomUUID()).isEmpty());
  }

  @Test
  public void test_destroy_session_FAIL() {
    final UUID userId = UUID.randomUUID();
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserCtx userCtx = new UserCtx(userId, username);
    this.sessionService.store(this.ctx, userCtx);

    this.sessionService.destroy(UUID.randomUUID());

    assertTrue(this.sessionService.session(userId).isPresent());
  }

  @Test
  public void test_release_session_OK() {
    final UUID userId = UUID.randomUUID();
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserCtx userCtx = new UserCtx(userId, username);
    this.sessionService.store(this.ctx, userCtx);

    final Optional<ZeptolabSession> found = this.sessionService.session(userId);

    assertTrue(found.isPresent());

    this.sessionService.destroy(userId);

    assertTrue(this.sessionService.session(userId).isEmpty());

    this.sessionService.release(found.get());

    assertTrue(this.sessionService.session(userId).isPresent());
  }

  @Test
  public void test_last_session_OK() {
    final UUID userId = UUID.randomUUID();
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserCtx userCtx = new UserCtx(userId, username);
    this.sessionService.store(this.ctx, userCtx);

    this.sessionService.destroy(userId);

    assertTrue(this.sessionService.lastSession(userId).isPresent());
  }

  @Test
  public void test_filter_sessions_by_channel_OK() {
    final UUID userId_1 = UUID.randomUUID();
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserCtx userCtx = new UserCtx(userId_1, username);
    this.sessionService.store(this.ctx, userCtx);

    final UUID userId_2 = UUID.randomUUID();
    final String username_2 = RandomStringUtils.randomAlphabetic(20);
    final UserCtx userCtx_2 = new UserCtx(userId_2, username_2);
    this.sessionService.store(this.ctx, userCtx_2);

    final UUID userId_3 = UUID.randomUUID();
    final String username_3 = RandomStringUtils.randomAlphabetic(20);
    final UserCtx userCtx_3 = new UserCtx(userId_3, username_3);
    this.sessionService.store(this.ctx, userCtx_3);

    final ZeptolabSession session_1 = this.sessionService.session(userId_1).get();
    final ZeptolabSession session_2 = this.sessionService.session(userId_2).get();
    final ZeptolabSession session_3 = this.sessionService.session(userId_3).get();

    final UUID channelId = UUID.randomUUID();
    session_1.updateChannel(new ChannelCtx(channelId, "channel-1"));
    session_2.updateChannel(new ChannelCtx(channelId, "channel-1"));
    session_3.updateChannel(new ChannelCtx(UUID.randomUUID(), "channel-2"));

    final List<ZeptolabSession> sessions = this.sessionService.filterByChannel(channelId);

    assertFalse(sessions.isEmpty());
    assertEquals(2, sessions.size());
    assertEquals(channelId, sessions.get(0).channelCtx().get().id);
    assertEquals(channelId, sessions.get(1).channelCtx().get().id);

    assertEquals("channel-1", sessions.get(0).channelCtx().get().name);
    assertEquals("channel-1", sessions.get(1).channelCtx().get().name);

  }
}
