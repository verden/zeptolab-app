package com.zeptolab.security;


import com.zeptolab.services.auth.SecurityService;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.exceptions.InvalidCredentialsException;
import com.zeptolab.services.exceptions.UserActiveSessionException;
import com.zeptolab.services.exceptions.UserNotFoundException;
import com.zeptolab.services.executors.LoginExecutor;
import com.zeptolab.services.users.UserService;
import com.zeptolab.services.users.models.UserRequest;
import com.zeptolab.services.users.models.UserResponse;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author William Arustamyan
 */

@SpringBootTest
public class SecurityServiceIntegrationTest {


  @Autowired
  private SecurityService securityService;

  @Autowired
  private LoginExecutor loginExecutor;

  @Autowired
  private UserService userService;

  private final ChannelHandlerContext ctx = Mockito.mock(ChannelHandlerContext.class);

  @Test
  public void test_authentication_OK() {
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserRequest request = new UserRequest(username, "password");
    final UserResponse response = this.userService.add(request);

    assertNotNull(response);
    assertEquals(request.username, response.username);
    assertNotNull(response.id);

    assertDoesNotThrow(() -> this.securityService.authenticate(new LoginCommand(request.username, request.password)));
  }

  @Test
  public void test_authentication_user_not_found_FAIL() {
    assertThrows(UserNotFoundException.class,
      () -> this.securityService.authenticate(new LoginCommand("fake username", "fake password"))
    );
  }

  @Test
  public void test_authentication_invalid_credentials_FAIL() {
    final UserRequest request = new UserRequest("username-2", "password");
    final UserResponse response = this.userService.add(request);

    assertNotNull(response);
    assertEquals(request.username, response.username);
    assertNotNull(response.id);

    assertThrows(InvalidCredentialsException.class, () -> this.securityService.authenticate(new LoginCommand(request.username, "wrong password")));
  }

  @Test
  public void test_authentication_user_already_logged_in_FAIL() {
    final LoginCommand command = new LoginCommand("username-3", "password");
    this.loginExecutor.execute(this.ctx, command);
    assertThrows(UserActiveSessionException.class, () -> securityService.authenticate(command));
  }
}
