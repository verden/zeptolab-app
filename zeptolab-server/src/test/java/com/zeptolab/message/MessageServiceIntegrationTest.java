package com.zeptolab.message;


import com.zeptolab.services.channels.ChannelService;
import com.zeptolab.services.channels.models.ChannelRequest;
import com.zeptolab.services.channels.models.ChannelResponse;
import com.zeptolab.services.messages.MessageService;
import com.zeptolab.services.messages.models.MessageRequest;
import com.zeptolab.services.messages.models.MessageResponse;
import com.zeptolab.services.users.UserService;
import com.zeptolab.services.users.models.UserRequest;
import com.zeptolab.services.users.models.UserResponse;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author William Arustamyan
 */


@SpringBootTest
public class MessageServiceIntegrationTest {

  @Autowired
  private MessageService messageService;

  @Autowired
  private ChannelService channelService;

  @Autowired
  private UserService userService;

  @Test
  public void test_create_message_OK() {
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserResponse user = this.userService.add(new UserRequest(username, "password"));
    final ChannelResponse channel = this.channelService.add(new ChannelRequest("channel"));

    final String message = "first message";
    final MessageRequest source = new MessageRequest(user.id, channel.id, message);
    final MessageResponse response = this.messageService.add(source);

    assertNotNull(response);
    assertNotNull(response.getUser());
    assertNotNull(response.getChannel());
    assertNotNull(response.getContent());

    assertEquals(user.id, response.getUser().id);
    assertEquals(channel.id, response.getChannel().id);
    assertEquals(message, response.getContent());

  }

  @Test
  public void test_find_last_messages_OK() {
    final String username = RandomStringUtils.randomAlphabetic(20);
    final String channel_1 = RandomStringUtils.randomAlphabetic(10);
    final UserResponse user = this.userService.add(new UserRequest(username, "password"));
    final ChannelResponse channel = this.channelService.add(new ChannelRequest(channel_1));

    final String message = "first message";
    final MessageRequest source = new MessageRequest(user.id, channel.id, message);
    this.messageService.add(source);

    final String secondMessage = "second message";
    final MessageRequest secondSource = new MessageRequest(user.id, channel.id, secondMessage);
    this.messageService.add(secondSource);

    final Set<MessageResponse> messages = this.messageService.findLastMessages(channel.id, 10);

    assertTrue(messages.stream().anyMatch(it -> it.getContent().equals(message)));
    assertTrue(messages.stream().anyMatch(it -> it.getContent().equals(secondMessage)));

  }


}
