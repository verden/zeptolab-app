package com.zeptolab.services;


import com.zeptolab.services.channels.ChannelService;
import com.zeptolab.services.channels.models.ChannelRequest;
import com.zeptolab.services.channels.models.ChannelResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author William Arustamyan
 */

@SpringBootTest
public class ChannelServiceIntegrationTest {

  @Autowired
  private ChannelService channelService;

  @Test
  public void test_create_channel_OK() {
    final ChannelRequest source = new ChannelRequest("channel");
    final ChannelResponse response = this.channelService.add(source);

    assertNotNull(response);
    assertEquals(source.name, response.name);
    assertNotNull(response.id);
  }

  @Test
  public void test_query_channel_by_name_OK() {
    final String channelName = "channel-1";
    final ChannelRequest source = new ChannelRequest(channelName);
    final ChannelResponse response = this.channelService.add(source);

    assertNotNull(response);
    assertEquals(source.name, response.name);
    assertNotNull(response.id);

    final Optional<ChannelResponse> found = this.channelService.channel(channelName);

    assertTrue(found.isPresent());
    assertEquals(response.id, found.get().id);
    assertEquals(channelName, found.get().name);
  }

  @Test
  public void test_available_channels_OK() {

    final String channel_3 = "channel-3";
    final ChannelRequest source_3 = new ChannelRequest(channel_3);
    this.channelService.add(source_3);

    final String channel_4 = "channel-4";
    final ChannelRequest source_4 = new ChannelRequest(channel_4);
    this.channelService.add(source_4);

    final Set<ChannelResponse> channels = this.channelService.channels();

    assertFalse(channels.isEmpty());
    assertTrue(channels.stream().anyMatch(it -> it.name.equals(channel_3)));
    assertTrue(channels.stream().anyMatch(it -> it.name.equals(channel_4)));
  }
}
