package com.zeptolab.user;


import com.zeptolab.services.users.UserService;
import com.zeptolab.services.users.models.UserRequest;
import com.zeptolab.services.users.models.UserResponse;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author William Arustamyan
 */


@SpringBootTest
public class UserServiceIntegrationTest {

  @Autowired
  private UserService userService;

  @Test
  public void test_create_new_user_OK() {
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserRequest source = new UserRequest(username, "password");
    final UserResponse response = this.userService.add(source);

    assertNotNull(response);
    assertEquals(source.username, response.username);
    assertNotNull(response.id);
  }

  @Test
  public void test_find_user_with_username_OK() {
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserRequest source = new UserRequest(username, "password");
    final UserResponse response = this.userService.add(source);

    assertNotNull(response);
    assertEquals(source.username, response.username);
    assertNotNull(response.id);

    final Optional<UserResponse> found = this.userService.find(username);

    assertTrue(found.isPresent());
    final UserResponse user = found.get();

    assertEquals(response.username, user.username);
    assertEquals(response.id, user.id);
  }

  @Test
  public void test_find_user_with_username_FAIL() {
    final String username = RandomStringUtils.randomAlphabetic(20);
    final UserRequest source = new UserRequest(username, "password");
    this.userService.add(source);

    final Optional<UserResponse> found = this.userService.find("non existing username");

    assertTrue(found.isEmpty());
  }
}
