package com.zeptolab.cli;


import com.zeptolab.services.cli.commands.CommandType;
import com.zeptolab.services.cli.commands.CommandsCommand;
import com.zeptolab.services.cli.commands.DisconnectCommand;
import com.zeptolab.services.cli.commands.JoinCommand;
import com.zeptolab.services.cli.commands.LeaveCommand;
import com.zeptolab.services.cli.commands.ListCommand;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.cli.commands.SendCommand;
import com.zeptolab.services.cli.commands.UsersCommand;
import com.zeptolab.services.cli.parsers.ZeptolabCLIParser;
import com.zeptolab.services.exceptions.InvalidCommandException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author William Arustamyan
 */


public class ZeptolabCLIParserTest {

  private final ZeptolabCLIParser cliParser = new ZeptolabCLIParser();

  @Test
  public void test_parse_login_command_OK() throws InvalidCommandException {
    final String strLoginCommand = "/login username password";

    assertDoesNotThrow(() -> cliParser.parse(strLoginCommand));
    assertInstanceOf(LoginCommand.class, this.cliParser.parse(strLoginCommand));

    final LoginCommand command = (LoginCommand) this.cliParser.parse(strLoginCommand);

    assertNotNull(command);
    assertEquals(command.type(), CommandType.LOGIN);
    assertEquals(command.username, "username");
    assertEquals(command.password, "password");
  }

  @Test
  public void test_parse_login_command_FAIL() {
    assertThrows(InvalidCommandException.class, () -> cliParser.parse("/login username"));
  }

  @Test
  public void test_parse_join_command_OK() throws InvalidCommandException {
    final String strJoinCommand = "/join channel";

    assertDoesNotThrow(() -> cliParser.parse(strJoinCommand));
    assertInstanceOf(JoinCommand.class, this.cliParser.parse(strJoinCommand));

    final JoinCommand command = (JoinCommand) this.cliParser.parse(strJoinCommand);

    assertNotNull(command);
    assertNotNull(command.type());
    assertEquals(command.type(), CommandType.JOIN);
    assertEquals(command.channelName, "channel");
  }

  @Test
  public void test_parse_join_command_FAIL() {
    assertThrows(InvalidCommandException.class, () -> cliParser.parse("/join channel name"));
    assertThrows(InvalidCommandException.class, () -> cliParser.parse("/join"));
  }

  @Test
  public void test_parse_leave_command_OK() throws InvalidCommandException {
    final String strLeaveCommand = "/leave";

    assertDoesNotThrow(() -> cliParser.parse(strLeaveCommand));
    assertInstanceOf(LeaveCommand.class, this.cliParser.parse(strLeaveCommand));

    final LeaveCommand command = (LeaveCommand) this.cliParser.parse(strLeaveCommand);

    assertNotNull(command);
    assertNotNull(command.type());
    assertEquals(command.type(), CommandType.LEAVE);
  }

  @Test
  public void test_parse_leave_command_FAIL() {
    assertThrows(InvalidCommandException.class, () -> cliParser.parse("/leave name"));
  }

  @Test
  public void test_parse_list_command_OK() throws InvalidCommandException {
    final String strListCommand = "/list";

    assertDoesNotThrow(() -> cliParser.parse(strListCommand));
    assertInstanceOf(ListCommand.class, this.cliParser.parse(strListCommand));

    final ListCommand command = (ListCommand) this.cliParser.parse(strListCommand);

    assertNotNull(command);
    assertNotNull(command.type());
    assertEquals(command.type(), CommandType.LIST);
  }

  @Test
  public void test_parse_list_command_FAIL() {
    assertThrows(InvalidCommandException.class, () -> cliParser.parse("/list name"));
  }

  @Test
  public void test_parse_users_command_OK() throws InvalidCommandException {
    final String strUsersCommand = "/users";

    assertDoesNotThrow(() -> cliParser.parse(strUsersCommand));
    assertInstanceOf(UsersCommand.class, this.cliParser.parse(strUsersCommand));

    final UsersCommand command = (UsersCommand) this.cliParser.parse(strUsersCommand);

    assertNotNull(command);
    assertNotNull(command.type());
    assertEquals(command.type(), CommandType.USERS);
  }

  @Test
  public void test_parse_users_command_FAIL() {
    assertThrows(InvalidCommandException.class, () -> cliParser.parse("/users name"));
  }

  @Test
  public void test_parse_send_command_OK() throws InvalidCommandException {
    final String message = "text message";
    final String strSendCommand = "/send " + message;

    assertDoesNotThrow(() -> cliParser.parse(strSendCommand));
    assertInstanceOf(SendCommand.class, this.cliParser.parse(strSendCommand));

    final SendCommand command = (SendCommand) this.cliParser.parse(strSendCommand);

    assertNotNull(command);
    assertEquals(command.message, message);
    assertNotNull(command.type());
    assertEquals(command.type(), CommandType.SEND);
  }

  @Test
  public void test_parse_send_command_FAIL() {
    assertThrows(InvalidCommandException.class, () -> cliParser.parse("/send"));
  }

  @Test
  public void test_parse_disconnect_command_OK() throws InvalidCommandException {
    final String strDisconnectCommand = "/disconnect";

    assertDoesNotThrow(() -> cliParser.parse(strDisconnectCommand));
    assertInstanceOf(DisconnectCommand.class, this.cliParser.parse(strDisconnectCommand));

    final DisconnectCommand command = (DisconnectCommand) this.cliParser.parse(strDisconnectCommand);

    assertNotNull(command);
    assertNotNull(command.type());
    assertEquals(command.type(), CommandType.DISCONNECT);
  }

  @Test
  public void test_parse_disconnect_command_FAIL() {
    assertThrows(InvalidCommandException.class, () -> cliParser.parse("/disconnect name"));
  }

  @Test
  public void test_parse_commands_command_OK() throws InvalidCommandException {
    final String strCommandsCommand = "/commands";

    assertDoesNotThrow(() -> cliParser.parse(strCommandsCommand));
    assertInstanceOf(CommandsCommand.class, this.cliParser.parse(strCommandsCommand));

    final CommandsCommand command = (CommandsCommand) this.cliParser.parse(strCommandsCommand);

    assertNotNull(command);
    assertNotNull(command.type());
    assertEquals(command.type(), CommandType.COMMANDS);
  }

  @Test
  public void test_parse_commands_command_FAIL() {
    assertThrows(InvalidCommandException.class, () -> cliParser.parse("/commands name"));
  }
}
