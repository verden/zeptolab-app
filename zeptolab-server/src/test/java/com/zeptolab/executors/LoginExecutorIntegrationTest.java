package com.zeptolab.executors;


import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.executors.LoginExecutor;
import com.zeptolab.services.executors.responses.LoginResponse;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author William Arustamyan
 */


@SpringBootTest
public class LoginExecutorIntegrationTest {

  @Autowired
  private LoginExecutor loginExecutor;

  private final ChannelHandlerContext ctx = Mockito.mock(ChannelHandlerContext.class);

  private final String username = RandomStringUtils.randomAlphabetic(5);

  @Test
  public void test_login_command_type_same_OK() {
    final LoginCommand command = new LoginCommand(username, "password");
    final LoginResponse response = this.loginExecutor.execute(this.ctx, command);

    assertNotNull(response);
    assertNotNull(response.type());
    assertEquals(command.type(), response.type());
  }

  @Test
  public void test_user_login_first_time_OK() {

    final LoginResponse response = this.loginExecutor.execute(this.ctx, new LoginCommand(username, "password"));

    assertNotNull(response);
    assertNotNull(response.userId);
    assertNotNull(response.username);
    assertNull(response.lastMessages);

    assertEquals(response.username, username);
    assertTrue(response.isNewUser);
  }


  @Test
  public void test_user_login_second_time_OK() {
    final LoginResponse firstResponse = this.loginExecutor.execute(this.ctx, new LoginCommand(username + "1", "password"));

    final LoginResponse secondResponse = this.loginExecutor.execute(this.ctx, new LoginCommand(username + "1", "password"));

    assertNotNull(firstResponse);
    assertNotNull(secondResponse);
    assertTrue(firstResponse.isNewUser);
    assertFalse(secondResponse.isNewUser);
    assertEquals(firstResponse.username, secondResponse.username);
    assertEquals(firstResponse.userId, secondResponse.userId);
  }
}
