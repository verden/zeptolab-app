package com.zeptolab.executors;


import com.zeptolab.services.cli.commands.JoinCommand;
import com.zeptolab.services.cli.commands.LeaveCommand;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.executors.JoinExecutor;
import com.zeptolab.services.executors.LeaveExecutor;
import com.zeptolab.services.executors.LoginExecutor;
import com.zeptolab.services.executors.responses.LoginResponse;
import com.zeptolab.services.session.SessionService;
import com.zeptolab.services.session.models.ZeptolabSession;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author William Arustamyan
 */


@SpringBootTest
public class LeaveExecutorIntegrationTest {

  @Autowired
  private LeaveExecutor leaveExecutor;

  @Autowired
  private LoginExecutor loginExecutor;

  @Autowired
  private JoinExecutor joinExecutor;

  @Autowired
  private SessionService sessionService;

  private final ChannelHandlerContext ctx = Mockito.mock(ChannelHandlerContext.class);

  @Test
  public void test_leave_from_channel_OK() {
    final String channelName = "channel";
    final String username = RandomStringUtils.randomAlphabetic(5);
    final LoginCommand command = new LoginCommand(username, "password");
    final LoginResponse response = this.loginExecutor.execute(this.ctx, command);

    final JoinCommand joinCommand = new JoinCommand(channelName);
    joinCommand.addUserId(response.userId);

    this.joinExecutor.execute(this.ctx, joinCommand);
    final Optional<ZeptolabSession> opSession = this.sessionService.session(response.userId);
    assertTrue(opSession.isPresent());
    final ZeptolabSession session = opSession.get();
    assertTrue(session.channelCtx().isPresent());

    final LeaveCommand leaveCommand = new LeaveCommand();
    leaveCommand.addUserId(response.userId);
    this.leaveExecutor.execute(ctx, leaveCommand);

    final Optional<ZeptolabSession> opSess = this.sessionService.session(response.userId);
    assertTrue(opSess.isPresent());

    assertTrue(opSess.get().channelCtx().isEmpty());
  }

  @Test
  public void test_leave_from_channel_FAIL() {
    final String username = RandomStringUtils.randomAlphabetic(5);
    final LoginCommand command = new LoginCommand(username, "password");
    final LoginResponse response = this.loginExecutor.execute(this.ctx, command);

    final LeaveCommand leaveCommand = new LeaveCommand();
    leaveCommand.addUserId(response.userId);
    this.leaveExecutor.execute(ctx, leaveCommand);

    final Optional<ZeptolabSession> opSess = this.sessionService.session(response.userId);
    assertTrue(opSess.isPresent());

    assertTrue(opSess.get().channelCtx().isEmpty());
  }
}
