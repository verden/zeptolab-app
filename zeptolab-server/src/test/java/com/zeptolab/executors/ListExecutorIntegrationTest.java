package com.zeptolab.executors;


import com.zeptolab.services.channels.ChannelService;
import com.zeptolab.services.channels.models.ChannelResponse;
import com.zeptolab.services.cli.commands.JoinCommand;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.executors.JoinExecutor;
import com.zeptolab.services.executors.LoginExecutor;
import com.zeptolab.services.executors.responses.LoginResponse;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author William Arustamyan
 */

@SpringBootTest
public class ListExecutorIntegrationTest {

  @Autowired
  private LoginExecutor loginExecutor;

  @Autowired
  private JoinExecutor joinExecutor;

  @Autowired
  private ChannelService channelService;

  private final ChannelHandlerContext ctx = Mockito.mock(ChannelHandlerContext.class);

  @Test
  public void test_list_command_OK() {
    final String channel_1 = RandomStringUtils.randomAlphabetic(10);
    final String username_1 = RandomStringUtils.randomAlphabetic(20);
    final LoginCommand command_1 = new LoginCommand(username_1, "password");
    final LoginResponse response_1 = this.loginExecutor.execute(this.ctx, command_1);

    final JoinCommand joinCommand_1 = new JoinCommand(channel_1);
    joinCommand_1.addUserId(response_1.userId);

    this.joinExecutor.execute(this.ctx, joinCommand_1);

    final String channel_2 = RandomStringUtils.randomAlphabetic(10);
    final String username_2 = RandomStringUtils.randomAlphabetic(20);
    final LoginCommand command_2 = new LoginCommand(username_2, "password");
    final LoginResponse response_2 = this.loginExecutor.execute(this.ctx, command_2);

    final JoinCommand joinCommand_2 = new JoinCommand(channel_2);
    joinCommand_2.addUserId(response_2.userId);

    this.joinExecutor.execute(this.ctx, joinCommand_2);

    final Set<ChannelResponse> channels = this.channelService.channels();

    assertFalse(channels.isEmpty());

    assertTrue(channels.stream().anyMatch(it -> it.name.equals(channel_1)));
    assertTrue(channels.stream().anyMatch(it -> it.name.equals(channel_2)));

  }
}
