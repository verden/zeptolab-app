package com.zeptolab.executors;


import com.zeptolab.services.cli.commands.JoinCommand;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.executors.JoinExecutor;
import com.zeptolab.services.executors.LoginExecutor;
import com.zeptolab.services.executors.responses.LoginResponse;
import com.zeptolab.services.session.SessionService;
import com.zeptolab.services.session.models.ZeptolabSession;
import io.netty.channel.ChannelHandlerContext;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author William Arustamyan
 */


@SpringBootTest
public class UsersExecutorIntegrationTest {

  @Autowired
  private LoginExecutor loginExecutor;

  @Autowired
  private JoinExecutor joinExecutor;

  @Autowired
  private SessionService sessionService;

  private final ChannelHandlerContext ctx = Mockito.mock(ChannelHandlerContext.class);

  @Test
  public void test_users_command_OK() {
    final String channelName = "channel";
    final String user_1 = "user-1";
    final String user_2 = "user-2";
    final LoginCommand command = new LoginCommand(user_1, "password");
    final LoginResponse response = this.loginExecutor.execute(this.ctx, command);

    final JoinCommand joinCommand = new JoinCommand(channelName);
    joinCommand.addUserId(response.userId);

    this.joinExecutor.execute(this.ctx, joinCommand);

    final LoginCommand command_1 = new LoginCommand(user_2, "password");
    final LoginResponse response_1 = this.loginExecutor.execute(this.ctx, command_1);

    final JoinCommand joinCommand_1 = new JoinCommand(channelName);
    joinCommand_1.addUserId(response_1.userId);

    this.joinExecutor.execute(this.ctx, joinCommand_1);


    final List<ZeptolabSession> zeptolabSessions = this.sessionService.filterByChannel(this.sessionService.session(response.userId).get().channelCtx().get().id);

    assertEquals(zeptolabSessions.size(), 2);
    assertEquals(zeptolabSessions.get(0).channelCtx().get().name, channelName);
    assertEquals(zeptolabSessions.get(1).channelCtx().get().name, channelName);
  }
}
