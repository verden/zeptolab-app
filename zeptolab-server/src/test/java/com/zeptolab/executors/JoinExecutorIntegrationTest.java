package com.zeptolab.executors;


import com.zeptolab.services.channels.ChannelService;
import com.zeptolab.services.channels.models.ChannelResponse;
import com.zeptolab.services.cli.commands.JoinCommand;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.executors.JoinExecutor;
import com.zeptolab.services.executors.LoginExecutor;
import com.zeptolab.services.executors.responses.LoginResponse;
import com.zeptolab.services.executors.responses.Void;
import com.zeptolab.services.session.SessionService;
import com.zeptolab.services.session.models.ChannelCtx;
import com.zeptolab.services.session.models.ZeptolabSession;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author William Arustamyan
 */


@SpringBootTest
public class JoinExecutorIntegrationTest {


  @Autowired
  private JoinExecutor joinExecutor;

  @Autowired
  private LoginExecutor loginExecutor;

  @Autowired
  private SessionService sessionService;

  @Autowired
  private ChannelService channelService;

  private final ChannelHandlerContext ctx = Mockito.mock(ChannelHandlerContext.class);


  @Test
  public void test_join_to_channel_OK() {
    final String channelName = "channel";
    final String username = RandomStringUtils.randomAlphabetic(5);
    final LoginCommand command = new LoginCommand(username, "password");
    final LoginResponse response = this.loginExecutor.execute(this.ctx, command);

    final JoinCommand joinCommand = new JoinCommand(channelName);
    joinCommand.addUserId(response.userId);

    final Void execute = this.joinExecutor.execute(this.ctx, joinCommand);

    assertNotNull(execute);

    final Optional<ZeptolabSession> opSession = this.sessionService.session(response.userId);

    assertTrue(opSession.isPresent());

    final ZeptolabSession session = opSession.get();

    assertTrue(session.channelCtx().isPresent());

    final ChannelCtx channelCtx = session.channelCtx().get();

    assertEquals(channelName, channelCtx.name);

    final Optional<ChannelResponse> opChannel = this.channelService.channel(channelName);

    assertTrue(opChannel.isPresent());

    final ChannelResponse channelResponse = opChannel.get();

    assertEquals(channelCtx.id, channelResponse.id);
  }

  @Test
  public void test_join_to_channel_FAIL() {
    final UUID randomUserId = UUID.randomUUID();
    final String channel = RandomStringUtils.randomAlphabetic(10);
    final JoinCommand joinCommand = new JoinCommand(channel);
    joinCommand.addUserId(randomUserId);

    this.joinExecutor.execute(this.ctx, joinCommand);

    assertTrue(this.sessionService.session(randomUserId).isEmpty());
    assertTrue(this.channelService.channel(channel).isEmpty());
  }
}
