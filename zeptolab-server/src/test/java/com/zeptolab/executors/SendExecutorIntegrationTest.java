package com.zeptolab.executors;


import com.zeptolab.persistence.entities.Message;
import com.zeptolab.persistence.repositories.MessageRepository;
import com.zeptolab.services.cli.commands.JoinCommand;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.cli.commands.SendCommand;
import com.zeptolab.services.executors.JoinExecutor;
import com.zeptolab.services.executors.LoginExecutor;
import com.zeptolab.services.executors.SendExecutor;
import com.zeptolab.services.executors.responses.LoginResponse;
import com.zeptolab.services.messages.MessageService;
import com.zeptolab.services.session.SessionService;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author William Arustamyan
 */

@SpringBootTest
public class SendExecutorIntegrationTest {

  @Autowired
  private LoginExecutor loginExecutor;

  @Autowired
  private JoinExecutor joinExecutor;

  @Autowired
  private SendExecutor sendExecutor;

  @Autowired
  private SessionService sessionService;

  @Autowired
  private MessageRepository messageRepository;

  private final ChannelHandlerContext ctx = Mockito.mock(ChannelHandlerContext.class);

  @Test
  public void test_send_message_OK() {
    final String channelName = "channel";
    final String username = RandomStringUtils.randomAlphabetic(5);
    final LoginCommand command = new LoginCommand(username, "password");
    final LoginResponse response = this.loginExecutor.execute(this.ctx, command);

    final JoinCommand joinCommand = new JoinCommand(channelName);
    joinCommand.addUserId(response.userId);

    this.joinExecutor.execute(this.ctx, joinCommand);

    final String strMsg = "test message";
    final SendCommand sendCommand = new SendCommand("test message");
    sendCommand.addUserId(response.userId);

    this.sendExecutor.execute(this.ctx, sendCommand);

    final Optional<Message> found = this.messageRepository.findByUser_Id(response.userId);

    assertTrue((found.isPresent()));

    final Message message = found.get();

    assertEquals(strMsg, message.getContent());
  }
}
