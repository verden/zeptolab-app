package com.zeptolab.executors;


import com.zeptolab.services.cli.commands.DisconnectCommand;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.executors.DisconnectExecutor;
import com.zeptolab.services.executors.LoginExecutor;
import com.zeptolab.services.executors.responses.LoginResponse;
import com.zeptolab.services.session.SessionService;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author William Arustamyan
 */


@SpringBootTest
public class DisconnectExecutorIntegrationTest {

  @Autowired
  private LoginExecutor loginExecutor;

  @Autowired
  private DisconnectExecutor disconnectExecutor;

  @Autowired
  private SessionService sessionService;

  private final ChannelHandlerContext ctx = Mockito.mock(ChannelHandlerContext.class);

  @Test
  public void test_disconnect_OK() {
    final String username = RandomStringUtils.randomAlphabetic(5);
    final String password = RandomStringUtils.randomAlphabetic(5);
    final LoginCommand command = new LoginCommand(username, password);
    final LoginResponse response = this.loginExecutor.execute(this.ctx, command);
    final DisconnectCommand disconnectCommand = new DisconnectCommand(response.userId);

    this.disconnectExecutor.execute(this.ctx, disconnectCommand);

    assertTrue(this.sessionService.session(response.userId).isEmpty());
  }
}
