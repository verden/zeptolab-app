package com.zeptolab;


import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ImportResource;

/**
 * @author William Arustamyan
 */

@SpringBootApplication
@ImportResource("classpath:/com/zeptolab/server/zeptolab-server-context.xml")
public class ZeptolabApplication {

  public static void main(String[] args) {
    new SpringApplicationBuilder(ZeptolabApplication.class)
      .bannerMode(Banner.Mode.OFF)
      .run(args);
  }
}
