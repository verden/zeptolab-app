package com.zeptolab.decoder;


import com.zeptolab.services.cli.commands.AbstractCommand;
import com.zeptolab.services.cli.commands.CommandType;
import com.zeptolab.services.cli.commands.CommandsCommand;
import com.zeptolab.services.cli.parsers.ZeptolabCLIParser;
import com.zeptolab.services.exceptions.InvalidCommandException;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author William Arustamyan
 */

public final class StrDecoder extends MessageToMessageDecoder<ByteBuf> {

  private static final Logger logger = LoggerFactory.getLogger(StrDecoder.class);

  private final ZeptolabCLIParser commandParser;

  public StrDecoder() {
    this.commandParser = new ZeptolabCLIParser();
  }

  @Override
  protected void decode(final ChannelHandlerContext ctx, final ByteBuf bb, final List<Object> items) {
    final String strCommand = StringUtils.normalizeSpace(bb.toString(StandardCharsets.UTF_8));
    logger.info("Decoding user request : {}", strCommand);
    if (StringUtils.isBlank(strCommand)) {
      logger.warn("Missing command parameter. Return");
      ctx.writeAndFlush(CommandType.commandGuide());
      return;
    }

    try {
      final AbstractCommand command = this.commandParser.parse(strCommand);
      if (command instanceof CommandsCommand) {
        ctx.writeAndFlush(CommandType.commandGuide());
      } else {
        items.add(command);
      }
    } catch (final InvalidCommandException e) {
      logger.error("Invalid command", e);
      ctx.writeAndFlush(e.getMessage());
    }
  }
}
