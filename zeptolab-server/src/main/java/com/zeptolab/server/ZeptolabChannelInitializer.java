package com.zeptolab.server;


import com.zeptolab.decoder.StrDecoder;
import com.zeptolab.handlers.LoginHandler;
import com.zeptolab.services.auth.SecurityService;
import com.zeptolab.services.executors.ExecutorRegistry;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.LineEncoder;
import io.netty.handler.codec.string.LineSeparator;
import io.netty.util.CharsetUtil;

/**
 * @author William Arustamyan
 */


public final class ZeptolabChannelInitializer extends ChannelInitializer<SocketChannel> {


  private final SecurityService secService;
  private final ExecutorRegistry executorRegistry;

  public ZeptolabChannelInitializer(final SecurityService secService,
                                    final ExecutorRegistry registry) {
    this.secService = secService;
    this.executorRegistry = registry;
  }

  @Override
  protected void initChannel(final SocketChannel channel) {
    //keep ordering
    channel.pipeline()
      .addLast(new LineEncoder(LineSeparator.UNIX, CharsetUtil.UTF_8))
      .addLast(new StrDecoder())
      .addLast(new LoginHandler(this.secService, this.executorRegistry));
  }
}
