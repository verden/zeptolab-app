package com.zeptolab.server;


import com.zeptolab.services.auth.SecurityService;
import com.zeptolab.services.executors.ExecutorRegistry;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;

/**
 * @author William Arustamyan
 */


@Component
public final class ZeptolabNettyServer {

  private static final Logger logger = LoggerFactory.getLogger(ZeptolabNettyServer.class);

  private final Integer serverPort;
  private final String host;

  private final NioEventLoopGroup parentGroup;
  private final NioEventLoopGroup childGroup;
  private final SecurityService securityService;
  private final ExecutorRegistry executorRegistry;
  private ChannelFuture future;

  public ZeptolabNettyServer(@Value("${zeptolab.server.port}") final Integer port,
                             @Value("${zeptolab.server.host}") final String host,
                             final SecurityService securityService,
                             final ExecutorRegistry executorRegistry) {
    this.host = host;
    this.serverPort = port;
    this.parentGroup = new NioEventLoopGroup();
    this.childGroup = new NioEventLoopGroup();
    this.securityService = securityService;
    this.executorRegistry = executorRegistry;
  }

  @PostConstruct
  private void _startServer() {
    logger.info("Starting Netty server");
    final ServerBootstrap bootstrap = new ServerBootstrap();

    bootstrap.group(this.parentGroup, this.childGroup)
      .channel(NioServerSocketChannel.class)
      .localAddress(new InetSocketAddress(this.host, this.serverPort))
      .childHandler(new ZeptolabChannelInitializer(this.securityService, this.executorRegistry))
      .childOption(ChannelOption.SO_KEEPALIVE, true);

    this.future = this.safeLoggingInterruption(bootstrap);
  }

  @PreDestroy
  private void _destroy() {
    logger.info("Stopping Netty server");
    try {
      parentGroup.shutdownGracefully().sync();
      childGroup.shutdownGracefully().sync();
      future.channel().closeFuture().sync();
    } catch (final Exception e) {
      logger.error("Server destroy failed", e);
      throw new RuntimeException(e);
    }
  }

  private ChannelFuture safeLoggingInterruption(final ServerBootstrap bts) {
    try {
      return bts.bind().sync();
    } catch (final InterruptedException e) {
      logger.error("Server Sync interrupted ", e);
      throw new RuntimeException("Server interrupted ...", e);
    }
  }

}
