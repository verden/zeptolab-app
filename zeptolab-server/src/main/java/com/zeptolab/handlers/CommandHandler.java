package com.zeptolab.handlers;


import com.zeptolab.services.cli.commands.AbstractCommand;
import com.zeptolab.services.cli.commands.DisconnectCommand;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.cli.commands.UserCommand;
import com.zeptolab.services.executors.ExecutorRegistry;
import com.zeptolab.services.executors.responses.LoginResponse;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 * @author William Arustamyan
 */


public final class CommandHandler extends ChannelInboundHandlerAdapter {

  private static final Logger logger = LoggerFactory.getLogger(CommandHandler.class);

  private final AbstractCommand command;
  private final ExecutorRegistry executorRegistry;
  private UUID userId;

  public CommandHandler(final AbstractCommand command, final ExecutorRegistry registry) {
    this.executorRegistry = registry;
    this.command = command;
  }

  @Override
  public void handlerAdded(final ChannelHandlerContext ctx) {
    logger.info("Accepted remote connection request");
    final LoginCommand loginCommand = (LoginCommand) this.command;

    final LoginResponse response = this.executorRegistry.execute(ctx, loginCommand);
    logger.debug("User with id: {} successfully logged in", response.userId);
    this.userId = response.userId;

    ctx.writeAndFlush(this.loginMessage(response));
  }

  private String loginMessage(final LoginResponse response) {
    if (response.isNewUser) {
      return String.format(
        "User with id: '%s' and username: '%s' successfully created",
        response.userId.toString(),
        response.username
      );
    }
    return String.format("Welcome back: '%s'", response.username) + "\n" + response.lastMessages;
  }

  @Override
  public void channelInactive(final ChannelHandlerContext ctx) {
    logger.info("Disconnecting from server");
    if (this.userId == null)
      throw new RuntimeException("Something go's wrong, unable to destroy session, user id is null ");

    this.executorRegistry.execute(ctx, new DisconnectCommand(this.userId));
  }

  @Override
  public void channelRead(final ChannelHandlerContext ctx, final Object msg) {
    final UserCommand cmd = (UserCommand) msg;
    cmd.addUserId(this.userId);
    logger.info("Accepted user command with user id : {}", cmd.userId());
    this.executorRegistry.execute(ctx, cmd);
  }
}
