package com.zeptolab.handlers;


import com.zeptolab.services.auth.SecurityService;
import com.zeptolab.services.cli.commands.LoginCommand;
import com.zeptolab.services.exceptions.AuthException;
import com.zeptolab.services.exceptions.InvalidCredentialsException;
import com.zeptolab.services.exceptions.UserActiveSessionException;
import com.zeptolab.services.executors.ExecutorRegistry;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author William Arustamyan
 */


public final class LoginHandler extends ChannelInboundHandlerAdapter {

  private static final Logger logger = LoggerFactory.getLogger(LoginHandler.class);

  private static final String not_authorized = "Not Authorized";

  private final SecurityService securityService;
  private final ExecutorRegistry executorRegistry;

  private Boolean isAuthenticated = false;


  public LoginHandler(final SecurityService securityService, final ExecutorRegistry registry) {
    this.executorRegistry = registry;
    this.securityService = securityService;
  }

  @Override
  public void channelRead(final ChannelHandlerContext ctx, final Object msg) {
    logger.info("Starts user login session");
    if (this.isAuthenticated) {
      logger.debug("User already authenticated");
      ctx.fireChannelRead(msg);
      return;
    }

    if (!(msg instanceof final LoginCommand command)) {
      ctx.writeAndFlush(not_authorized);
      return;
    }

    //sry for this :(
    try {
      this.securityService.authenticate(command);
    } catch (final AuthException ex) {
      if (ex instanceof InvalidCredentialsException) {
        logger.error("invalid credentials");
        this.isAuthenticated = false;
        ctx.writeAndFlush(ex.getMessage());
        return;
      }
      if (ex instanceof UserActiveSessionException) {
        logger.warn("User already has active session");
        ctx.writeAndFlush(ex.getMessage());
        ctx.close();
        return;
      }
    }

    this.isAuthenticated = true;
    logger.info("User authentication success. Passing request to command executors.");
    ctx.pipeline()
      .addLast(
        new CommandHandler(command, this.executorRegistry)
      );
  }
}
