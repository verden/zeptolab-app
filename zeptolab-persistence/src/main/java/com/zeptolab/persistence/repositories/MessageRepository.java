package com.zeptolab.persistence.repositories;


import com.zeptolab.persistence.entities.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

@Repository
public interface MessageRepository extends JpaRepository<Message, UUID> {

  List<Message> findAllByChannel_Id(UUID channelId, Pageable pageable);

  Optional<Message> findByUser_Id(UUID userId);

}
