package com.zeptolab.persistence.repositories;


import com.zeptolab.persistence.entities.Channel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * @author William Arustamyan
 */


@Repository
public interface ChannelRepository extends JpaRepository<Channel, UUID> {

  default Optional<Channel> findChannel(String name) {
    return findByNameAndDeletedIsNull(name);
  }

  Optional<Channel> findByNameAndDeletedIsNull(String name);

  default Set<Channel> findChannels() {
    return findAllByDeletedIsNull();
  }

  Set<Channel> findAllByDeletedIsNull();
}
