package com.zeptolab.persistence.repositories;


import com.zeptolab.persistence.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

  default Optional<User> findActiveUser(String username) {
    return findByUsernameAndDeletedIsNull(username);
  }

  Optional<User> findByUsernameAndDeletedIsNull(String username);
}
