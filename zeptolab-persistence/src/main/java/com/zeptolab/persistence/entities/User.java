package com.zeptolab.persistence.entities;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author William Arustamyan
 */


@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "t_users")
public class User extends BaseEntity {

  @Column(name = "username", nullable = false)
  private String username;

  @Column(name = "password", nullable = false)
  private String password;

  public User(final String username, final String password) {
    this.username = username;
    this.password = password;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;

    if (o == null || getClass() != o.getClass()) return false;

    final User user = (User) o;

    return new EqualsBuilder()
      .appendSuper(super.equals(o))
      .append(this.id, user.id)
      .append(this.username, user.username)
      .append(this.password, user.password)
      .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
      .appendSuper(super.hashCode())
      .append(this.id)
      .append(this.username)
      .append(this.password)
      .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
      .append("id", this.id)
      .append("username", this.username)
      .append("created", this.created)
      .append("updated", this.updated)
      .append("deleted", this.deleted)
      .toString();
  }
}
