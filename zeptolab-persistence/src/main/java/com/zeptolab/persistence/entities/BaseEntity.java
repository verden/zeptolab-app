package com.zeptolab.persistence.entities;


import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreRemove;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDateTime;
import java.util.UUID;


/**
 * @author William Arustamyan
 */

@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  protected UUID id;

  @Column(name = "created", nullable = false)
  protected LocalDateTime created;

  @Column(name = "updated", nullable = false)
  protected LocalDateTime updated;

  @Column(name = "deleted")
  protected LocalDateTime deleted;

  @PrePersist
  protected void onCreate() {
    this.created = LocalDateTime.now();
    this.updated = this.created;
  }

  @PreRemove
  public void onRemove() {
    this.deleted = LocalDateTime.now();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;

    if (o == null || getClass() != o.getClass()) return false;

    final BaseEntity that = (BaseEntity) o;

    return new EqualsBuilder()
      .append(this.id, that.id)
      .append(this.created, that.created)
      .append(this.updated, that.updated)
      .append(this.deleted, that.deleted)
      .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
      .append(this.id)
      .append(this.created)
      .append(this.updated)
      .append(this.deleted)
      .toHashCode();
  }
}

