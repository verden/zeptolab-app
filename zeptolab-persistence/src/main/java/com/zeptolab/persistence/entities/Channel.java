package com.zeptolab.persistence.entities;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author William Arustamyan
 */

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "t_channels")
public class Channel extends BaseEntity {

  @Column(name = "name", nullable = false)
  private String name;

  public Channel(final String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;

    if (!(o instanceof Channel channel)) return false;

    return new EqualsBuilder()
      .appendSuper(super.equals(o))
      .append(this.name, channel.name)
      .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
      .appendSuper(super.hashCode())
      .append(this.name)
      .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
      .append("id", this.id)
      .append("name", this.name)
      .append("created", this.created)
      .append("updated", this.updated)
      .append("deleted", this.deleted)
      .toString();
  }
}
