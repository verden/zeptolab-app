package com.zeptolab.persistence.entities;


import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author William Arustamyan
 */

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "t_messages")
public class Message extends BaseEntity {

  @Column(name = "content", nullable = false)
  private String content;
  ;
  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
  @JoinColumn(name = "channel_id", referencedColumnName = "id", nullable = false)
  private Channel channel;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
  @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
  private User user;


  public Message(final String content, final Channel channel, final User user) {
    this.content = content;
    this.channel = channel;
    this.user = user;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;

    if (!(o instanceof Message message)) return false;

    return new EqualsBuilder()
      .appendSuper(super.equals(o))
      .append(this.content, message.content)
      .append(this.channel.id, message.channel.id)
      .append(this.user.id, message.user.id)
      .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
      .appendSuper(super.hashCode())
      .append(this.content)
      .append(this.channel.id)
      .append(this.user.id)
      .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
      .append("id", this.id)
      .append("content", this.content.substring(0, 20))
      .append("channelId", this.channel.id)
      .append("userId", this.user.id)
      .append("created", this.created)
      .append("updated", this.updated)
      .append("deleted", this.deleted)
      .toString();
  }
}
