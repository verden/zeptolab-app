create table t_users
(
  id       uuid        not null
    constraint pk_t_users primary key,

  username varchar(50) not null,
  password varchar(50) not null,

  created  timestamp   not null default current_timestamp,
  updated  timestamp   not null default current_timestamp,
  deleted  timestamp
);

create table t_channels
(
  id      uuid        not null
    constraint pk_t_channels primary key,

  name    varchar(50) not null,

  created timestamp   not null default current_timestamp,
  updated timestamp   not null default current_timestamp,
  deleted timestamp
);

create table t_messages
(
  id         uuid                            not null
    constraint pk_t_messages primary key,

  content    varchar(2000)                   not null,

  channel_id uuid references t_channels (id) not null,
  user_id    uuid references t_users (id)    not null,

  created    timestamp                       not null default current_timestamp,
  updated    timestamp                       not null default current_timestamp,
  deleted    timestamp
)